require('dotenv').config()

// TODO: middleware
// When responding with an access token, the server must also include the additional Cache-Control:
// no-store and Pragma: no-cache HTTP headers to ensure clients do not cache this request.

const mongoose = require('mongoose')
mongoose
  .connect(process.env.MONGO_CONNECTION_STRING, {
    useNewUrlParser: true,
    useUnifiedTopology: true
  })
  .then(() => {
    mongoose.connection.on('error', console.error.bind(console, 'connection error:')) // TODO: log error

    const express = require('express')
    const helmet = require('helmet')
    const cors = require('cors')

    const app = express()
    app.use(helmet())
    app.use(cors())
    app.use(express.json())

    app.options('*', cors())
    app.use(require('./middlewares/authentication'))

    app.use(require('./backgrounds/backgroundController'))
    app.use(require('./races/raceController'))
    app.use(require('./users/userController'))
    app.get('/', (_, res) => res.send('Oubliette API'))

    const port = process.env.PORT
    app.listen(port, () => console.log(`Oubliette API listening on port ${port}`))
  })
  .catch(
    e => console.error(e) // TODO: log error
  )
