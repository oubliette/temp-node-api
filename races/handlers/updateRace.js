const { v4: uuidv4 } = require('uuid')
const Validator = require('validatorjs')
const Race = require('../models/Race')
const RaceViewModel = require('../raceViewModel')
const rules = require('../rules')
const { cleanTrim } = require('../../stringHelper')

const getTrait = trait => ({
  abilities: trait.abilities,
  custom: trait.custom,
  description: cleanTrim(trait.description),
  id: trait.id || uuidv4(),
  name: cleanTrim(trait.name),
  speeds: trait.speeds
})

module.exports = async (req, res) => {
  try {
    let validator = new Validator(req.body, rules)
    if (validator.fails()) {
      return res.status(400).json(validator.errors)
    }

    const race = await Race.findOne({ id: req.params.id })
    if (!race) {
      return res.status(404).send()
    } else if (race.account !== req.account.id) {
      return res.status(403).send()
    }

    race.custom = req.body.custom
    race.description = cleanTrim(req.body.description)
    race.name = cleanTrim(req.body.name)
    race.names = req.body.names
    race.subRaces = req.body.subRaces
      ? req.body.subRaces.map(subRace => ({
          custom: subRace.custom,
          description: cleanTrim(subRace.description),
          id: subRace.id || uuidv4(),
          name: cleanTrim(subRace.name),
          names: subRace.names,
          traits: subRace.traits ? subRace.traits.map(getTrait) : []
        }))
      : []
    race.traits = req.body.traits ? req.body.traits.map(getTrait) : []
    race.updated = new Date()
    await race.save()

    res.json(new RaceViewModel(race))
  } catch (e) {
    console.error(e) // TODO: log error
    res.status(500).send()
  }
}
