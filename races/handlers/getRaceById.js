const Race = require('../models/Race')
const RaceViewModel = require('../raceViewModel')

module.exports = async (req, res) => {
  try {
    const race = await Race.findOne({ id: req.params.id })
    if (!race) {
      return res.status(404).send()
    } else if (race.account !== req.account.id) {
      return res.status(403).send()
    }
    res.json(new RaceViewModel(race))
  } catch (e) {
    console.error(e) // TODO: log error
    res.status(500).send()
  }
}
