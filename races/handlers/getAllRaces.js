const Race = require('../models/Race')

module.exports = async (req, res) => {
  try {
    const races = await Race.find({ account: req.account.id })
    res.json(
      races.map(race => ({
        id: race.id,
        name: race.name,
        updated: race.updated || race.created
      }))
    )
  } catch (e) {
    console.error(e) // TODO: log error
    res.status(500).send()
  }
}
