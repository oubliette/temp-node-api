const { v4: uuidv4 } = require('uuid')
const Validator = require('validatorjs')
const Race = require('../models/Race')
const RaceViewModel = require('../raceViewModel')
const rules = require('../rules')
const { cleanTrim } = require('../../stringHelper')

const getTrait = trait => ({
  abilities: trait.abilities,
  custom: trait.custom,
  description: cleanTrim(trait.description),
  id: uuidv4(),
  name: cleanTrim(trait.name),
  speeds: trait.speeds
})

module.exports = async (req, res) => {
  try {
    let validator = new Validator(req.body, rules)
    if (validator.fails()) {
      return res.status(400).json(validator.errors)
    }

    const race = new Race({
      account: req.account.id,
      created: new Date(),
      custom: req.body.custom,
      description: cleanTrim(req.body.description),
      id: uuidv4(),
      name: cleanTrim(req.body.name),
      names: req.body.names,
      subRaces: req.body.subRaces
        ? req.body.subRaces.map(subRace => ({
            custom: subRace.custom,
            description: cleanTrim(subRace.description),
            id: uuidv4(),
            name: cleanTrim(subRace.name),
            names: subRace.names,
            traits: subRace.traits ? subRace.traits.map(getTrait) : []
          }))
        : [],
      traits: req.body.traits ? req.body.traits.map(getTrait) : []
    })
    await race.save()

    res.set('Location', `/races/${race.id}`)
    res.status(201).json(new RaceViewModel(race))
  } catch (e) {
    console.error(e) // TODO: log error
    res.status(500).send()
  }
}
