module.exports = class {
  constructor(model) {
    this.created = model.created
    this.custom = model.custom
    this.description = model.description
    this.id = model.id
    this.name = model.name
    this.names = model.names
    this.subRaces = model.subRaces
    this.traits = model.traits
    this.updated = model.updated
  }
}
