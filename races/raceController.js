const router = require('express').Router()

router.delete('/races/:id', require('./handlers/deleteRace'))
router.get('/races', require('./handlers/getAllRaces'))
router.get('/races/:id', require('./handlers/getRaceById'))
router.post('/races', require('./handlers/createRace'))
router.put('/races/:id', require('./handlers/updateRace'))

module.exports = router
