const mongoose = require('mongoose')

const schema = new mongoose.Schema({
  account: String,
  created: Date,
  custom: {},
  description: String,
  id: String,
  name: String,
  names: {},
  subRaces: [],
  traits: [],
  updated: Date
})

module.exports = mongoose.model('Race', schema)
