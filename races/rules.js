module.exports = {
  description: 'string|max:10000',
  name: 'required|string|max:100',
  names: {
    description: 'string|max:1000'
  },
  'subRaces.*.description': 'string|max:1000',
  'subRaces.*.name': 'required|string|max:100',
  'subRaces.*.traits.*.description': 'string|max:1000',
  'subRaces.*.traits.*.name': 'string|max:100',
  'traits.*.description': 'string|max:1000',
  'traits.*.name': 'required|string|max:100'
}
