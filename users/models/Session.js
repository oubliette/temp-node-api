const mongoose = require('mongoose')

const schema = new mongoose.Schema({
  account: String,
  created: Date,
  id: String,
  keyHash: String,
  updated: Date
})

module.exports = mongoose.model('Session', schema)
