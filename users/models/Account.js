const mongoose = require('mongoose')

const schema = new mongoose.Schema({
  confirmed: Date,
  created: Date,
  email: String,
  id: String,
  name: String,
  password: {
    changed: Date,
    hash: String
  },
  updated: Date,
  username: String
})

module.exports = mongoose.model('Account', schema)
