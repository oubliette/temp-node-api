const mongoose = require('mongoose')

const schema = new mongoose.Schema({
  account: String,
  created: Date,
  id: String,
  updated: Date
})

module.exports = mongoose.model('PasswordReset', schema)
