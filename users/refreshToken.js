const { parse, stringify } = require('uuid')

module.exports = class {
  constructor(id, key) {
    this.id = id
    this.key = key
  }
  static parse(s) {
    const buffer = Buffer.from(s, 'base64')
    const id = stringify(buffer.slice(0, 16))
    const key = buffer.slice(16)
    return new this(id, key)
  }
  toString() {
    const buffer = Buffer.concat([parse(this.id), this.key])
    return buffer.toString('base64')
  }
}
