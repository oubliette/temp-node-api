const router = require('express').Router()

router.get('/user/profile', require('./handlers/getProfile'))
router.post('/user/change-password', require('./handlers/changePassword'))
router.post('/user/confirm', require('./handlers/confirm'))
router.post('/user/login', require('./handlers/login'))
router.post('/user/logout', require('./handlers/logout'))
router.post('/user/recover-password', require('./handlers/recoverPassword'))
router.post('/user/register', require('./handlers/register'))
router.post('/user/renew', require('./handlers/renew'))
router.post('/user/reset-password', require('./handlers/resetPassword'))

module.exports = router
