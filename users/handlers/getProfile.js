module.exports = (req, res) =>
  res.json({
    confirmed: req.account.confirmed,
    created: req.account.created,
    email: req.account.email,
    name: req.account.name,
    passwordChanged: req.account.password.changed,
    updated: req.account.updated
  })
