const { compare, hash } = require('bcrypt')
const Validator = require('validatorjs')
require('../passwordRule')

const rules = {
  current: 'required|string',
  password: 'required|string|min:8|password'
}

const saltRounds = parseInt(process.env.BCRYPT_SALT_ROUNDS)

module.exports = async (req, res) => {
  try {
    let validator = new Validator(req.body, rules)
    if (validator.fails()) {
      return res.status(400).json(validator.errors)
    }

    const account = req.account
    if (!(await compare(req.body.current, account.password.hash))) {
      return res.status(400).json({ code: 'invalid_credentials' })
    }

    account.password.changed = new Date()
    account.password.hash = await hash(req.body.password, saltRounds)
    await account.save()
    res.status(204).send()
  } catch (e) {
    console.error(e) // TODO: log error
    res.status(500).send()
  }
}
