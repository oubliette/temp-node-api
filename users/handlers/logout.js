const { compare } = require('bcrypt')
const Validator = require('validatorjs')
const RefreshToken = require('../refreshToken')
const Session = require('../models/Session')

const rules = {
  refresh_token: 'required|string'
}

module.exports = async (req, res) => {
  try {
    const validator = new Validator(req.body, rules)
    if (validator.fails()) {
      return res.status(400).json(validator.errors)
    }

    const refreshToken = RefreshToken.parse(req.body.refresh_token)
    const session = await Session.findOne({ id: refreshToken.id })
    if (!session || !(await compare(refreshToken.key.toString('base64'), session.keyHash))) {
      return res.status(400).json({ code: 'invalid_credentials' })
    }

    await Session.deleteOne({ _id: session._id })
    res.status(204).send()
  } catch (e) {
    console.error(e) // TODO: log error
    res.status(500).send()
  }
}
