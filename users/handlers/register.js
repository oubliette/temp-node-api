const { hash } = require('bcrypt')
const { v4: uuidv4 } = require('uuid')
const Validator = require('validatorjs')
const Account = require('../models/Account')
require('../passwordRule')
const { cleanTrim } = require('../../stringHelper')

const rules = {
  email: 'required|string|max:255|email',
  name: 'required|string|max:255',
  password: 'required|string|min:8|password'
}

const saltRounds = parseInt(process.env.BCRYPT_SALT_ROUNDS)

module.exports = async (req, res) => {
  try {
    const validator = new Validator(req.body, rules)
    if (validator.fails()) {
      return res.status(400).json(validator.errors)
    }

    const username = req.body.email.toLowerCase()
    let account = await Account.findOne({ username })
    if (!account) {
      account = new Account({
        created: new Date(),
        email: req.body.email,
        id: uuidv4(),
        name: cleanTrim(req.body.name),
        password: {
          hash: await hash(req.body.password, saltRounds)
        },
        username
      })
      await account.save()
      // TODO: send confirmation message
    }

    res.status(204).send()
  } catch (e) {
    console.error(e) // TODO: log error
    res.status(500).send()
  }
}
