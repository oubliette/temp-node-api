const { verify } = require('jsonwebtoken')
const Validator = require('validatorjs')
const Account = require('../models/Account')

const rules = { token: 'required|string' }

const secret = process.env.JWT_SECRET

module.exports = async (req, res) => {
  try {
    const validator = new Validator(req.body, rules)
    if (validator.fails()) {
      return res.status(400).json(validator.errors)
    }
    const token = verify(req.body.token, secret)
    if (!token.sub) {
      return res.status(400).json({ code: 'missing_sub' })
    }
    const account = await Account.findOne({ id: token.sub })
    if (!account) {
      return res.status(400).json({ code: 'invalid_credentials' })
    } else if (!account.confirmed) {
      account.confirmed = new Date()
      await account.save()
    }
    res.status(204).send()
  } catch (e) {
    if (e.name === 'JsonWebTokenError' || e.name === 'TokenExpiredError') {
      return res.status(400).json({ code: 'invalid_credentials' })
    }
    console.error(e) // TODO: log error
    res.status(500).send()
  }
}
