const { compare, hash } = require('bcrypt')
const { randomBytes } = require('crypto')
const { sign } = require('jsonwebtoken')
const { v4: uuidv4 } = require('uuid')
const Validator = require('validatorjs')
const Account = require('../models/Account')
const RefreshToken = require('../refreshToken')
const Session = require('../models/Session')

const rules = {
  password: 'required|string',
  username: 'required|string'
}

const expires_in = parseInt(process.env.TOKEN_LIFETIME)
const refreshKeyLength = parseInt(process.env.REFRESH_KEY_LENGTH)
const saltRounds = parseInt(process.env.BCRYPT_SALT_ROUNDS)
const secret = process.env.JWT_SECRET
const token_type = process.env.TOKEN_TYPE

module.exports = async (req, res) => {
  try {
    const validator = new Validator(req.body, rules)
    if (validator.fails()) {
      return res.status(400).json(validator.errors)
    }

    const account = await Account.findOne({ username: req.body.username.toLowerCase() })
    if (!account || !(await compare(req.body.password, account.password.hash))) {
      return res.status(400).json({ code: 'invalid_credentials' })
    } else if (!account.confirmed) {
      return res.status(400).json({ code: 'not_confirmed' })
    }

    const key = randomBytes(refreshKeyLength)
    const session = new Session({
      account: account.id,
      created: new Date(),
      id: uuidv4(),
      keyHash: await hash(key.toString('base64'), saltRounds)
    })
    await session.save()

    const access_token = sign({ sub: account.id }, secret, { expiresIn: expires_in })
    const refresh_token = new RefreshToken(session.id, key).toString()
    res.json({ access_token, expires_in, refresh_token, token_type })
  } catch (e) {
    console.error(e) // TODO: log error
    res.status(500).send()
  }
}
