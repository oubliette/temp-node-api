const { hash } = require('bcrypt')
const { verify } = require('jsonwebtoken')
const Validator = require('validatorjs')
const Account = require('../models/Account')
const PasswordReset = require('../models/PasswordReset')
require('../passwordRule')

const rules = {
  password: 'required|string|min:8|password',
  token: 'required|string'
}

const saltRounds = parseInt(process.env.BCRYPT_SALT_ROUNDS)
const secret = process.env.JWT_SECRET

module.exports = async (req, res) => {
  try {
    const validator = new Validator(req.body, rules)
    if (validator.fails()) {
      return res.status(400).json(validator.errors)
    }

    const token = verify(req.body.token, secret)
    if (!token.sub) {
      return res.status(400).json({ code: 'missing_sub' })
    }

    const passwordReset = await PasswordReset.findOne({ id: token.sub })
    if (!passwordReset) {
      return res.status(400).json({ code: 'invalid_credentials' })
    }
    await PasswordReset.deleteOne({ _id: passwordReset._id })

    const account = await Account.findOne({ id: passwordReset.account })
    if (!account) {
      return res.status(400).json({ code: 'invalid_credentials' })
    }
    account.password.changed = new Date()
    account.password.hash = await hash(req.body.password, saltRounds)
    await account.save()

    res.status(204).send()
  } catch (e) {
    if (e.name === 'JsonWebTokenError' || e.name === 'TokenExpiredError') {
      return res.status(400).json({ code: 'invalid_credentials' })
    }
    console.error(e) // TODO: log error
    res.status(500).send()
  }
}
