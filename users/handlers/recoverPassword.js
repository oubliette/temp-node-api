const { v4: uuidv4 } = require('uuid')
const Validator = require('validatorjs')
const Account = require('../models/Account')
const PasswordReset = require('../models/PasswordReset')

const rules = {
  username: 'required|string'
}

module.exports = async (req, res) => {
  try {
    const validator = new Validator(req.body, rules)
    if (validator.fails()) {
      return res.status(400).json(validator.errors)
    }

    const username = req.body.username.toLowerCase()
    const account = await Account.findOne({ username })
    if (account) {
      let passwordReset = await PasswordReset.findOne({ account: account.id })
      if (passwordReset) {
        passwordReset.updated = new Date()
      } else {
        passwordReset = new PasswordReset({
          account: account.id,
          created: new Date(),
          id: uuidv4()
        })
      }
      await passwordReset.save()
      // TODO: send recovery message
    }

    res.status(204).send()
  } catch (e) {
    console.error(e) // TODO: log error
    res.status(500).send()
  }
}
