const { compare, hash } = require('bcrypt')
const { randomBytes } = require('crypto')
const { sign } = require('jsonwebtoken')
const Validator = require('validatorjs')
const RefreshToken = require('../refreshToken')
const Session = require('../models/Session')

const rules = {
  refresh_token: 'required|string'
}

const expires_in = parseInt(process.env.TOKEN_LIFETIME)
const refreshKeyLength = parseInt(process.env.REFRESH_KEY_LENGTH)
const saltRounds = parseInt(process.env.BCRYPT_SALT_ROUNDS)
const secret = process.env.JWT_SECRET
const token_type = process.env.TOKEN_TYPE

module.exports = async (req, res) => {
  try {
    const validator = new Validator(req.body, rules)
    if (validator.fails()) {
      return res.status(400).json(validator.errors)
    }

    const refreshToken = RefreshToken.parse(req.body.refresh_token)
    const session = await Session.findOne({ id: refreshToken.id })
    if (!session || !(await compare(refreshToken.key.toString('base64'), session.keyHash))) {
      return res.status(400).json({ code: 'invalid_credentials' })
    }

    refreshToken.key = randomBytes(refreshKeyLength)
    session.keyHash = await hash(refreshToken.key.toString('base64'), saltRounds)
    session.updated = new Date()
    await session.save()

    const access_token = sign({ sub: session.account }, secret, { expiresIn: expires_in })
    const refresh_token = refreshToken.toString()
    res.json({ access_token, expires_in, refresh_token, token_type })
  } catch (e) {
    console.error(e) // TODO: log error
    res.status(500).send()
  }
}
