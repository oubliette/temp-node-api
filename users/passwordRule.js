const Validator = require('validatorjs')

Validator.register(
  'password',
  value => {
    let digits = 0,
      lowers = 0,
      others = 0,
      uppers = 0
    for (let i = 0; i < value.length; i++) {
      if (value[i].toLowerCase() !== value[i].toUpperCase()) {
        if (value[i].toLowerCase() === value[i]) {
          lowers++
        } else {
          uppers++
        }
      } else if (!isNaN(value[i])) {
        digits++
      } else {
        others++
      }
    }
    return digits && lowers && others && uppers
  },
  'The :attribute must contain at least an uppercase letter, a lowercase letter, a digit and any special character.'
)
