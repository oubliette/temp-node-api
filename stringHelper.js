module.exports = {
  cleanTrim(s) {
    return typeof s === 'string' ? s.trim() : null
  }
}
