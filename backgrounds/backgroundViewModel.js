module.exports = class {
  constructor(model) {
    this.created = model.created
    this.custom = model.custom
    this.description = model.description
    this.equipment = model.equipment
    this.extraLanguages = model.extraLanguages
    this.features = model.features
    this.id = model.id
    this.name = model.name
    this.skills = model.skills
    this.tools = model.tools
    this.updated = model.updated
  }
}
