const router = require('express').Router()

router.delete('/backgrounds/:id', require('./handlers/deleteBackground'))
router.get('/backgrounds', require('./handlers/getAllBackgrounds'))
router.get('/backgrounds/:id', require('./handlers/getBackgroundById'))
router.post('/backgrounds', require('./handlers/createBackground'))
router.put('/backgrounds/:id', require('./handlers/updateBackground'))

module.exports = router
