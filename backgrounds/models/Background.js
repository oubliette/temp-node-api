const mongoose = require('mongoose')

const schema = new mongoose.Schema({
  account: String,
  created: Date,
  custom: {},
  description: String,
  equipment: [],
  extraLanguages: Number,
  features: [],
  id: String,
  name: String,
  skills: [],
  tools: [],
  updated: Date
})

module.exports = mongoose.model('Background', schema)
