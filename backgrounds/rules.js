const skills = require('../skills.json')

module.exports = {
  description: 'string|max:10000',
  equipment: 'array',
  extraLanguages: 'numeric|min:0',
  name: 'required|string|max:100',
  skills: `in:${skills.join(',')}`,
  tools: 'array',
  'features.*.description': 'string|max:1000',
  'features.*.name': 'required|string|max:100'
}
