const Background = require('../models/Background')

module.exports = async (req, res) => {
  try {
    const backgrounds = await Background.find({ account: req.account.id })
    res.json(
      backgrounds.map(background => ({
        id: background.id,
        name: background.name,
        updated: background.updated || background.created
      }))
    )
  } catch (e) {
    console.error(e) // TODO: log error
    res.status(500).send()
  }
}
