const { v4: uuidv4 } = require('uuid')
const Validator = require('validatorjs')
const Background = require('../models/Background')
const BackgroundViewModel = require('../backgroundViewModel')
const rules = require('../rules')
const { cleanTrim } = require('../../stringHelper')

module.exports = async (req, res) => {
  try {
    let validator = new Validator(req.body, rules)
    if (validator.fails()) {
      return res.status(400).json(validator.errors)
    }

    const background = new Background({
      account: req.account.id,
      created: new Date(),
      custom: req.body.custom,
      description: cleanTrim(req.body.description),
      equipment: req.body.equipment,
      extraLanguages: req.body.extraLanguages,
      features: req.body.features
        ? req.body.features.map(feature => ({
            custom: feature.custom,
            description: cleanTrim(feature.description),
            id: uuidv4(),
            name: cleanTrim(feature.name)
          }))
        : [],
      id: uuidv4(),
      name: cleanTrim(req.body.name),
      skills: req.body.skills,
      tools: req.body.tools
    })
    await background.save()

    res.set('Location', `/backgrounds/${background.id}`)
    res.status(201).json(new BackgroundViewModel(background))
  } catch (e) {
    console.error(e) // TODO: log error
    res.status(500).send()
  }
}
