const Background = require('../models/Background')
const BackgroundViewModel = require('../backgroundViewModel')

module.exports = async (req, res) => {
  try {
    const background = await Background.findOne({ id: req.params.id })
    if (!background) {
      return res.status(404).send()
    } else if (background.account !== req.account.id) {
      return res.status(403).send()
    }
    await Background.deleteOne({ _id: background._id })
    res.json(new BackgroundViewModel(background))
  } catch (e) {
    console.error(e) // TODO: log error
    res.status(500).send()
  }
}
