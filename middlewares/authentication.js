const { verify } = require('jsonwebtoken')
const Account = require('../users/models/Account')

const publicRoutes = ['/', '/user/confirm', '/user/login', '/user/recover-password', '/user/register', '/user/renew', '/user/reset-password']
const secret = process.env.JWT_SECRET
const type = process.env.TOKEN_TYPE

// TODO: confirm access_token must not be used to authenticate

// TODO: implement access_token black list

module.exports = async (req, res, next) => {
  try {
    if (!publicRoutes.includes(req.url)) {
      const authorization = req.header('Authorization')
      if (!authorization) {
        return res.status(401).json({ code: 'missing_auth' })
      }
      const values = authorization.split(' ')
      if (values.length !== 2) {
        return res.status(401).json({ code: 'invalid_auth' })
      } else if (values[0].toLowerCase() !== type.toLowerCase()) {
        return res.status(401).json({ code: 'not_supported' })
      }
      const token = verify(values[1], secret)
      if (!token.sub) {
        return res.status(401).json({ code: 'missing_sub' })
      }
      const account = await Account.findOne({ id: token.sub })
      if (!account) {
        return res.status(401).json({ code: 'invalid_sub' })
      }
      req.account = account
    }
    next()
  } catch (e) {
    if (e.name === 'JsonWebTokenError' || e.name === 'TokenExpiredError') {
      return res.status(401).json({ code: 'invalid_credentials' })
    }
    console.error(e) // TODO: log error
    res.status(500).send()
  }
}
